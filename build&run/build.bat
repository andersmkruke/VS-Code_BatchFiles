:: If the first command line argument is "run", the output file will run after building 
@echo off
IF NOT "%1"=="run" (
    echo -Build Only-
    GOTO BuildOnly
)

echo -Build and Run-

:BuildOnly
:: Update to appropriate path and option (x64/x86/amd64/amd64_x86)
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
:: Specify name of output executable
set output=program.exe
:: Add /Zi to create debug symbols and pdb
set compilerflags=/EHsc
set linkerflags=/out:%output%
cl.exe %compilerflags% @sources /I @includes /link %linkerflags%

IF NOT "%1"=="run" GOTO End

:Run
echo(
echo Running:
echo ...
%output%

:End