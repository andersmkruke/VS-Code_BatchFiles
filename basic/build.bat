:: Replace with appropriate path and option (x64/x86/amd64/amd64_x86)
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
:: Add /Zi to generate debug symbols
set compilerflags=/EHsc
set linkerflags=/out:multifile.exe
:: Replace [src] with source files (.cpp/.lib) and [include] with include paths
cl.exe %compilerflags% [src] /I [include] /link %linkerflags%