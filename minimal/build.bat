@echo off
:: Replace with correct path. Use x64, x86, amd64 or amd64_x86 according to your system and/or build target:
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
set compilerflags=/Od /Zi /EHsc
set linkerflags=/OUT:program.exe
:: Replace [*cpp/*lib] with files to be compiled:
cl.exe %compilerflags% [*.cpp/*.lib] /link %linkerflags%