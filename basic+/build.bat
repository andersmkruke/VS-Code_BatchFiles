@echo off
:: Update to appropriate path and option (x64/x86/amd64/amd64_x86)
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
:: Add /Zi to create debug symbols
set compilerflags=/EHsc
set linkerflags=/out:program.exe
:: Source files and include directories are kept in separate files 
cl.exe %compilerflags% @sources /I @includes /link %linkerflags%